#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include "image_ppm.h"

using namespace std;


int main(int argc, char* argv[]){
	char cNomImgLue[250];
	int histo[256];
	int nH, nW, nTaille;
	

	/* On check si l'utilisateur c'est trompé en ne donnant pas
	 tous les parametres lors de l'execution. Si c'est le cas 
	 on lui rappelle avec un print */
	
	if (argc != 2) 
	
	{
		printf("Usage: ImageIn");
	}

	/* Initialisation du tableau à 0 avec 256 cases correspondant 
	aux niveaux de gris */

	for (int i = 0 ; i<256; i++)
	{
		histo[i]=0;
	}

	/* Recup du param de l'exec et on le plance dans cNomImgLue */

	sscanf (argv[1], "%s", cNomImgLue);
	
	OCTET *ImgIn;

	lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   	nTaille = nH * nW;

   	allocation_tableau(ImgIn, OCTET, nTaille);
   	lire_image_pgm(cNomImgLue, ImgIn, nH*nW);
   	for (int i = 0; i < nH; i++) 
   	/* 
   	histo[i] position du tableau correspondant à la ieme valeur de gris
   	On met dans histo[i] (donc le niveau de gris) le nombre de pixel qui à ce niveau de gris
   	ex : histo[128]=30 - 30 pixels qui ont comme niveau de gris 128.
	*/
   	{
   		for (int j = 0; j < nW; j++)
   		{
   			histo[ImgIn[i*nW+j]]++;
   		}
   	}
   	
   	/* On ecrit dans le fichier histo dat le couple i et histo[i] */
   	
   	ofstream outfile ("histo.dat");

   	for (int i=0;i<256;i++)
   	{
   		
   		outfile<<i << " " << histo[i]<<endl;

   	}
   	outfile.close();

   	return 1;
}