#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include "image_ppm.h"

using namespace std;


int main(int argc, char* argv[]){
	char cNomImgLue[250];
	int histoR[256], histoG[256], histoB[256];
	int nH, nW, nTaille, nR, nG, nB;
	

	/* On check si l'utilisateur c'est trompé en ne donnant pas
	 tous les parametres lors de l'execution. Si c'est le cas 
	 on lui rappelle avec un print */
	
	if (argc != 2) 
	
	{
		printf("Usage: ImageIn");
	}

	/* Initialisation du tableau à 0 avec 256 cases correspondant 
	aux niveaux de gris */

	for (int i = 0 ; i<256; i++)
	{
		histoR[i]=0;
		histoB[i]=0;
    	histoG[i]=0;
	}

	/* Recup du param de l'exec et on le plance dans cNomImgLue */

	sscanf (argv[1], "%s", cNomImgLue);
	
	OCTET *ImgIn;

	lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   	nTaille = nH * nW;

   	int nTaille3 = nTaille * 3;
   	allocation_tableau(ImgIn, OCTET, nTaille3);
   	lire_image_ppm(cNomImgLue, ImgIn, nH*nW);
   	
   	for (int i=0; i < nTaille3; i+=3)
    {
        nR = ImgIn[i];
        nG = ImgIn[i+1];
        nB = ImgIn[i+2];
 		histoR[nR]++;
 		histoG[nG]++;
 		histoB[nB]++;
    }
   	
   	
   	ofstream outfile ("histoColor.dat");

   	for (int i=0;i<256;i++)
   	{
   		
   		outfile<<i << " " << histoR[i]<<" " << histoG[i]<< " " << histoB[i]<<endl;

   	}
   	outfile.close();

   	return 1;
}