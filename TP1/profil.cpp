#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include "image_ppm.h"

using namespace std;


int main(int argc, char* argv[])
{
  
  char cNomImgLue[250];
  int idChoix;
  int nH, nW, id, nTaille;
  int histo[256];
  OCTET *ImgIn;

  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%d", &idChoix);
  sscanf (argv[3],"%d", &id);

  for(int i = 0; i < 256; i++)
  {
    histo[i] = 0;
  } 

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
  
  if (idChoix == 0)
  {
    for (int j = 0; j < nW; j++)
   		{
   			histo[ImgIn[id*nW+j]]++;
   		}
  }	
  else
  {
    for (int i=0; i < nH; i++)
    {
      histo[ImgIn[i*nW+id]]++;
    }
  }
  
  ofstream outfile ("profil.dat");  
  printf("Colonne/Ligne | Valeur");
  for (int i=0; i<256; i++)
    {
      cout << i << " | " << histo[i] << endl;
      outfile<<i << " " << histo[i]<<endl;
    }

    outfile.close();

  return 1;
}